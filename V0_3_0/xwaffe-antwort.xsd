<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xwaffe-behoerden="http://www.xwaffe.de/schemata/behoerden/V2_2/"
           xmlns:xwaffe-kern="http://www.xwaffe.de/schemata/kern/V2_2/"
           targetNamespace="http://www.xwaffe.de/schemata/behoerden/V2_2/"
           version="2.2"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XWaffe – Standard für die elektronische Datenübermittlung im deutschen Waffenwesen</nameLang>
            <nameKurz>XWaffe</nameKurz>
            <nameTechnisch>xwaffe</nameTechnisch>
            <kennung>urn:xoev-de:bmi:standard:xwaffe</kennung>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Durch Beschluss der IMK und die Aufnahme in das Errichtungsgesetz zum NWR / die einschlägige Verwaltungsvorschriften soll der Standard eine ebenenübergreifende Verbindlichkeit erhalten. In der Version 1.0 des Standards werden die Prozesse der Phase I des Vorhabens "Deutschland Online - Nationales Waffenregister" durch den Standard unterstützt. Der Standard ist in folgenden Versionen für Weiterentwicklungen offen. Denkbar wäre hier die Modellierung weiterer Objekte im Bereich Waffen z.B. im Kontext polizeilicher kriminaltechnologischer Ermittlungen.</beschreibung>
         </standard>
         <versionStandard>
            <version>2.2</version>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Durch Beschluss der IMK und die Aufnahme in das Errichtungsgesetz zum NWR / die einschlägige Verwaltungsvorschriften soll der Standard eine ebenenübergreifende Verbindlichkeit erhalten. Der XML-Datenaustauschstandard XWaffe wurde im Zeitraum Dezember 2011 bis April 2012 weiterentwickelt. Schwerpunkt der Weiterentwicklung zur Zwischenversion 1.2.2 waren die Änderungsbedarfe für den Standard XWaffe, die sich im Rahmen der Erarbeitung zum Entwurf eines Gesetzes zur Errichtung eines Nationalen Waffenregisters (NWRG) im Jahr 2011 und den damit verbundenen Abstimmungen mit den Ressorts und den erfolgten Stellungnahmen der Länder ergeben haben. Die Versionen 1.3, 1.4 und 1.4.1 berücksichtigen jeweils Erkenntnisse aus dem Betrieb ab 2012, ab 2013 bzw. ab 2015. Die Version 1.5 betrachtet die technische Aufteilung des Standards in einen allgemeinen Kernbereich (wiederverwendbare Bausteinen innerhalb von XWaffe) sowie den fachlichen Bereich des Nachrichtenaustausch zwischen ÖWS und der Zentralen Komponente . Des Weiteren erfolgte die Abbildung waffenrechtlicher Geschäftsvorfälle der Waffenbehörden im Zusammenspiel von ÖWS und der Zentralen Komponente: insbesondere die Abbildung von Waffenteilen. Ebenso wurden komplexe Nachrichten und die Registrierung statusverändernder Aktivitäten in einem neuen Aktivitätsobjekt ausgestaltet. Die Versionen 1.5.1 und 1.5.2 berücksichtigen Erkenntnisse aus der Umsetzung im Jahr 2017. Der Standard XWaffe wurde mit der Version 2.0 um die Kommunikation der Händler und Hersteller mit der Kopfstelle des NWR erweitert. Zusätzlich wurde die bestehende Kommunikation der ÖWS mit der Zentralen Komponente um die Kommunikation mit der Kopfstelle erweitert. Die Versionen 2.1, 2.1.1 und 2.2 berücksichtigen Erkenntnisse aus der Umsetzung in den Jahren Jahr 2017 und 2018.</beschreibung>
            <versionXOEVProfil>2.1.0_p3</versionXOEVProfil>
            <versionXOEVHandbuch>2.1.0</versionXOEVHandbuch>
            <versionXGenerator>2.6.1</versionXGenerator>
            <versionModellierungswerkzeug>18.0</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
      <xs:documentation>Dieser Nachrichtentyp wird vom NWR an das ÖWS versendet und enthält im allgemeinen die Antwort auf Such- und Rechercheanfragen. Die Nachricht enhält das Ergebnis einer Suche und Recherche sowie Meta-Informationen zum Ergebnis der Suche und Recherche.</xs:documentation>
   </xs:annotation>
   <xs:include schemaLocation="xwaffe-baukasten-behoerden.xsd"/>
   <xs:import schemaLocation="xwaffe-baukasten.xsd"
              namespace="http://www.xwaffe.de/schemata/kern/V2_2/"/>
   <xs:element name="antwort.auskunft.402">
      <xs:annotation>
         <xs:appinfo>
            <autor>Registerbehörde NWR</autor>
            <leser>Abfrageberechtigte Systeme auf Basis §10 NWRG</leser>
         </xs:appinfo>
         <xs:documentation>Liefert Objekte (Voreintrag, Waffenverweis, Waffe, natürliche Person, nichtnatürliche Person, Erlaubnis, Behörde) als "gefundenes Objekt" sowie die zuständige Behörde im Rahmen eines "Suchergebnis" zurück. Auslöser dieser Antwortnachricht (Lieferung) des Registers ist die abfrage.auskunft.401.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="ergebnis"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:SuchergebnisType">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die Daten des NWR als Antwort auf eine Suchabfrage vom ÖWS ab. Bei Personendaten wird für jede PersonID ein Element vom Typ "Suchergebnis" geliefert. Wird die gleiche Person mit gleicher PersonenstammID in unterschiedlichen Behörden verwaltet (mit unterschiedlicher PersonID) so werden diese in separaten Suchergebnis-Objekten zurückgeliefert.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="istTeilmenge" minOccurs="0" type="xs:boolean">
               <xs:annotation>
                  <xs:documentation>Wurde durch das abfragende System die gewünschte Anzahl der zu liefernden Datensätze angegeben, gibt dieses Feld die Rückinformation, dass dieses nur eine Teilmenge der gefundenen Daten ist. Ja = Es liegen noch weitere Daten vor Nein = Die gelieferten Datensätze sind die letzten der Ergebnismenge</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="fehlermeldung"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:FehlerRegisterType">
               <xs:annotation>
                  <xs:documentation>Dieses Feld wird für Fehlermeldungen verwendet. In der vorliegenden Version von XWaffe werden nur Fehlermeldungen vom Register zum ÖWS definiert. Im Allgemeinen kann zwischen verschiedenen Prüfungstypen/Fehlerarten unterschieden werden: Entitätsprüfungen (prüfen, ob alle MUSS-Entitäten vorhanden sind und Entitäten in der richtigen Anzahl vorhanden sind), Feldprüfungen (prüfen ob MUSS-Felder und Felder in der richtigen Anzahl vorhanden sind), Datentypprüfungen (prüfen auf den richtigen Typ und die richtige Länge), Katalogwertprüfungen (prüfen, ob der Wert in der zugehörigen aktuellen Katalogtabelle enthalten ist), Meldekombinationsprüfungen (prüfen auf Kriterien, die von anderen Werten abhängig sind, z.B. ob ein Datum hinter einem anderen übergebenen Datum liegt).</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="anzahlTreffer" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Wurde durch das abfragende System die gewünschte Anzahl der zu liefernden Datensätze angegeben, gibt dieses Feld die Rückinformation, dass dieses nur eine Teilmenge der gefundenen Daten ist. Ja = Es liegen noch weitere Daten vor Nein = Die gelieferten Datensätze sind die letzten der Ergebnismenge</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="antwort.lesen.406">
      <xs:annotation>
         <xs:appinfo>
            <autor>Registerbehörde NWR</autor>
            <leser>Abfrageberechtigte Systeme auf Basis §10 NWRG</leser>
         </xs:appinfo>
         <xs:documentation>Liefert genau ein Hauptobjekt (Voreintrag, Waffenverweis, Waffe, natürliche Person, nichtnatürliche Person, Erlaubnis, Behörde) auf Grundlage einer angefragten ID so zurück, wie es im Register registriert ist. Auslöser dieser Antwortnachricht (Lieferung) des Registers sind z.B. abfrage.lesen.405.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="inhalt"
                        minOccurs="0"
                        type="xwaffe-behoerden:InhaltsdatenType">
               <xs:annotation>
                  <xs:documentation>Das Objekt Inhaltsdaten umfasst die von Lesevorgängen durch das Register gelieferten "registriertenObjekte".</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="fehlermeldung"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:FehlerRegisterType">
               <xs:annotation>
                  <xs:documentation>Dieses Feld wird für Fehlermeldungen verwendet. In der vorliegenden Version von XWaffe werden nur Fehlermeldungen vom Register zum ÖWS definiert. Im Allgemeinen kann zwischen verschiedenen Prüfungstypen/Fehlerarten unterschieden werden: Entitätsprüfungen (prüfen, ob alle MUSS-Entitäten vorhanden sind und Entitäten in der richtigen Anzahl vorhanden sind), Feldprüfungen (prüfen ob MUSS-Felder und Felder in der richtigen Anzahl vorhanden sind), Datentypprüfungen (prüfen auf den richtigen Typ und die richtige Länge), Katalogwertprüfungen (prüfen, ob der Wert in der zugehörigen aktuellen Katalogtabelle enthalten ist), Meldekombinationsprüfungen (prüfen auf Kriterien, die von anderen Werten abhängig sind, z.B. ob ein Datum hinter einem anderen übergebenen Datum liegt).</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="antwort.recherche.412">
      <xs:annotation>
         <xs:appinfo>
            <autor>Registerbehörde NWR</autor>
            <leser>Abfrageberechtigte Systeme auf Basis §10 NWRG</leser>
         </xs:appinfo>
         <xs:documentation>Liefert Objekte (Erlaubnis, Person, Waffe, Waffenverweis, Voreintrag) als "registriertes Objekt" im Rahmen eines "Rechercheergebnis" zurück. Auslöser dieser Antwortnachricht (Lieferung) des Registers ist die Nachricht abfrage.recherche.411.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="rechercheergebnis"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:RechercheergebnisType">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die Daten des NWR als Antwort auf eine Rechercheabfrage vom ÖWS ab.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="fehlermeldung"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:FehlerRegisterType">
               <xs:annotation>
                  <xs:documentation>Dieses Feld wird für Fehlermeldungen verwendet. In der vorliegenden Version von XWaffe werden nur Fehlermeldungen vom Register zum ÖWS definiert. Im Allgemeinen kann zwischen verschiedenen Prüfungstypen/Fehlerarten unterschieden werden: Entitätsprüfungen (prüfen, ob alle MUSS-Entitäten vorhanden sind und Entitäten in der richtigen Anzahl vorhanden sind), Feldprüfungen (prüfen ob MUSS-Felder und Felder in der richtigen Anzahl vorhanden sind), Datentypprüfungen (prüfen auf den richtigen Typ und die richtige Länge), Katalogwertprüfungen (prüfen, ob der Wert in der zugehörigen aktuellen Katalogtabelle enthalten ist), Meldekombinationsprüfungen (prüfen auf Kriterien, die von anderen Werten abhängig sind, z.B. ob ein Datum hinter einem anderen übergebenen Datum liegt).</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="anzahlTreffer" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Wurde durch das abfragende System die gewünschte Anzahl der zu liefernden Datensätze angegeben, gibt dieses Feld die Rückinformation, dass dieses nur eine Teilmenge der gefundenen Daten ist. Ja = Es liegen noch weitere Daten vor Nein = Die gelieferten Datensätze sind die letzten der Ergebnismenge</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="antwort.hinweis.422">
      <xs:annotation>
         <xs:appinfo>
            <autor>Registerbehörde NWR</autor>
            <leser>Abfrageberechtigte Systeme auf Basis §10 NWRG</leser>
         </xs:appinfo>
         <xs:documentation>Rückantwort zur Abfrage der Ordnungsnummern (NWRID) gespeicherter Hinweise.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="abfragendeBehoerdeID"
                        minOccurs="0"
                        type="xwaffe-kern:String.behoerdenIDType">
               <xs:annotation>
                  <xs:documentation>Die Ordnungsnummer (NWRID) der abfragenden Behörde, für die Hinweise bestimmt sind.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="registrierterHinweisID"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-kern:String.nwrIDType">
               <xs:annotation>
                  <xs:documentation>Die Ordnungsnummer (NWRID) eines für die abfragende Behörde registrierten Hinweises.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="fehlermeldung"
                        minOccurs="0"
                        maxOccurs="unbounded"
                        type="xwaffe-behoerden:FehlerRegisterType">
               <xs:annotation>
                  <xs:documentation>Diese Feld wird für Fehlermeldungen verwendet. In der vorliegenden Version von XWaffe werden nur Fehlermeldungen vom Register zum ÖWS definiert. Im Allgemeinen kann zwischen verschiedenen Prüfungstypen/Fehlerarten unterschieden werden: Entitä tsprüfungen (prüfen ob alle MUSS-Entitäten vorhanden sind und Entitäten in der richtigen Anzahl vorhanden sind) Feldprüfungen (prüfen ob MUSS-Felder vorhanden sind und Felder in der richtigen Anzahl vorhanden sind) Datentypprüfungen (prüfen auf den richtigen Typ und die richtige Länge) Katalogwertprüfungen (prüfen ob der Wert in der zugehörigen aktuellen Katalogtabelle enthalten ist) Meldekombinationsprüfungen (prüft auf Kriterien die von anderen Werten abhängig sind, z.B. ob ein Datum hinter einem anderen übergebenen Datum liegt)</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
</xs:schema>
