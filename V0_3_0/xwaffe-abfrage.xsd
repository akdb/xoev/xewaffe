<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xwaffe-behoerden="http://www.xwaffe.de/schemata/behoerden/V2_2/"
           targetNamespace="http://www.xwaffe.de/schemata/behoerden/V2_2/"
           version="2.2"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XWaffe – Standard für die elektronische Datenübermittlung im deutschen Waffenwesen</nameLang>
            <nameKurz>XWaffe</nameKurz>
            <nameTechnisch>xwaffe</nameTechnisch>
            <kennung>urn:xoev-de:bmi:standard:xwaffe</kennung>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Durch Beschluss der IMK und die Aufnahme in das Errichtungsgesetz zum NWR / die einschlägige Verwaltungsvorschriften soll der Standard eine ebenenübergreifende Verbindlichkeit erhalten. In der Version 1.0 des Standards werden die Prozesse der Phase I des Vorhabens "Deutschland Online - Nationales Waffenregister" durch den Standard unterstützt. Der Standard ist in folgenden Versionen für Weiterentwicklungen offen. Denkbar wäre hier die Modellierung weiterer Objekte im Bereich Waffen z.B. im Kontext polizeilicher kriminaltechnologischer Ermittlungen.</beschreibung>
         </standard>
         <versionStandard>
            <version>2.2</version>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Durch Beschluss der IMK und die Aufnahme in das Errichtungsgesetz zum NWR / die einschlägige Verwaltungsvorschriften soll der Standard eine ebenenübergreifende Verbindlichkeit erhalten. Der XML-Datenaustauschstandard XWaffe wurde im Zeitraum Dezember 2011 bis April 2012 weiterentwickelt. Schwerpunkt der Weiterentwicklung zur Zwischenversion 1.2.2 waren die Änderungsbedarfe für den Standard XWaffe, die sich im Rahmen der Erarbeitung zum Entwurf eines Gesetzes zur Errichtung eines Nationalen Waffenregisters (NWRG) im Jahr 2011 und den damit verbundenen Abstimmungen mit den Ressorts und den erfolgten Stellungnahmen der Länder ergeben haben. Die Versionen 1.3, 1.4 und 1.4.1 berücksichtigen jeweils Erkenntnisse aus dem Betrieb ab 2012, ab 2013 bzw. ab 2015. Die Version 1.5 betrachtet die technische Aufteilung des Standards in einen allgemeinen Kernbereich (wiederverwendbare Bausteinen innerhalb von XWaffe) sowie den fachlichen Bereich des Nachrichtenaustausch zwischen ÖWS und der Zentralen Komponente . Des Weiteren erfolgte die Abbildung waffenrechtlicher Geschäftsvorfälle der Waffenbehörden im Zusammenspiel von ÖWS und der Zentralen Komponente: insbesondere die Abbildung von Waffenteilen. Ebenso wurden komplexe Nachrichten und die Registrierung statusverändernder Aktivitäten in einem neuen Aktivitätsobjekt ausgestaltet. Die Versionen 1.5.1 und 1.5.2 berücksichtigen Erkenntnisse aus der Umsetzung im Jahr 2017. Der Standard XWaffe wurde mit der Version 2.0 um die Kommunikation der Händler und Hersteller mit der Kopfstelle des NWR erweitert. Zusätzlich wurde die bestehende Kommunikation der ÖWS mit der Zentralen Komponente um die Kommunikation mit der Kopfstelle erweitert. Die Versionen 2.1, 2.1.1 und 2.2 berücksichtigen Erkenntnisse aus der Umsetzung in den Jahren Jahr 2017 und 2018.</beschreibung>
            <versionXOEVProfil>2.1.0_p3</versionXOEVProfil>
            <versionXOEVHandbuch>2.1.0</versionXOEVHandbuch>
            <versionXGenerator>2.6.1</versionXGenerator>
            <versionModellierungswerkzeug>18.0</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
      <xs:documentation>Nachrichtentypen für die Such- und Rechercheabfragen von Informationen aus dem Zentralen Waffenregister. Dieser Nachrichtentyp kann von den ÖWS oder von berechtigten Stellen zur Abfrage verwendet werden.</xs:documentation>
   </xs:annotation>
   <xs:include schemaLocation="xwaffe-baukasten-behoerden.xsd"/>
   <xs:element name="abfrage.auskunft.401">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 NWRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Suchanfrage des ÖWS oder berechtigter Stellen an das NWR</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="suchprofil" type="xwaffe-behoerden:SuchprofilType">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstigen berechtigten Stellen durchgeführten Suchabfragedaten ab. Übergreifende Suchparameter werden im Suchprofil aufgenommen, fachspezifische (z.B. Tag der Geburt) in den Datenfeldern pro gesuchtes Objekt (Person, etc.).</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.lesen.405">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 NWRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage von gespeicherten Datensätzen anhand ihrer Ordnungsnummer (NWRID)</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="leseprofil"
                        type="xwaffe-behoerden:LeseprofilType"
                        form="unqualified">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstige berechtigte Stellen durchgeführten Leseabfragedaten ab.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.recherche.411">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 NWRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage zur Ermittlung der einem Hauptobjekt (Person, Erlaubnis, Waffe, Waffenteil) untergeordneten Datensätze</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="rechercheprofil" type="xwaffe-behoerden:RechercheprofilType">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstige berechtigte Stellen durchgeführten Recherchen mit den zugehörigen Abfragedaten ab. Die Rechercheparameter werden über das Rechercheprofil aufgenommen. Die Recherche erfolgt dabei grundsätzlich über ObjektIDs. In den Datenfeldern können pro zu filterndem Objekt (Erlaubnis, Waffe, etc.) zusätzlich Angaben enthalten sein, die die Recherche eingrenzen. Zurückgeliefert werden bei einer Recherche mit: PersonID: Die vorhandenen Erlaubnisobjekte (Anträge und erteilte Erlaubnisse als Erlaubnisinhaber / wirtschaftlich berechtigter Kaufmann oder Unternehmer bzw. als Verantwortlicher oder Erlaubnisinhaber nach §21 Absatz 1 Satz 1 WaffG) oder die im Erwerbsprozess an diese Person befindlichen Waffen oder Waffenteilobjekte. PersonenstammID: Die vorhandenen Personenobjekte. ErlaubnisID: Die vorhandenen Waffen-, Waffenteil, Waffenverweis-, VoreintragsWaffenteil oder Voreintragsobjekte. WaffenID: Die vorhandenen Waffenteil-, Aktivitäts-, Waffenverweis- oder Voreintragsobjekte. WaffenteilID: Die vorhandenen Waffenteil-, Aktivitäts-, Waffenverweis- oder VoreintragWaffenteilobjekte.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.hinweis.421">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 NWRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage der Ordnungsnummern (NWRID) der für eine WaffB hinterlegten Hinweise.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:NachrichtenkopfType">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="hinweisprofil" type="xwaffe-behoerden:HinweisprofilType">
               <xs:annotation>
                  <xs:documentation>Daten zur Einschränkung der gewünschten Hinweismenge.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
</xs:schema>
