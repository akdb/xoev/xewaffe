<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xewaffe="https://gitlab.opencode.de/akdb/xoev/xewaffe/-/raw/main/V1_1_0"
           targetNamespace="https://gitlab.opencode.de/akdb/xoev/xewaffe/-/raw/main/V1_1_0"
           version="1.1.0"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XeWaffe</nameLang>
            <nameKurz>XeWaffe</nameKurz>
            <nameTechnisch>xewaffe</nameTechnisch>
            <kennung>urn:xoev-de:stmd:standard:xewaffe</kennung>
            <beschreibung>Der Standard XeWaffe deckt den Bedarf des Betreibers sowie den weiterer Stakeholder
am Transport von digitalen (OZG-) Anträgen im Kontext waffenrechtlicher Erlaubnisse ab.</beschreibung>
         </standard>
         <versionStandard>
            <version>1.1.0</version>
            <beschreibung>Die mit diesem Release vorgenommenen Änderungen sind dem Anhang F der Spezifikation zu entnehmen.</beschreibung>
            <versionXOEVProfil>3.0.0</versionXOEVProfil>
            <versionXOEVHandbuch>3.0</versionXOEVHandbuch>
            <versionXGenerator>3.1.0</versionXGenerator>
            <versionModellierungswerkzeug>2022x</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
   </xs:annotation>
   <xs:include schemaLocation="xewaffe-baukasten.xsd"/>
   <xs:element name="vorgang.transportieren.2010">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>§ 8 WaffG;§ 14 (2) WaffG;§ 14 (3) WaffG</rechtsgrundlage>
            <title>Vorgang (Antrag/Fall/Bescheid) transportieren</title>
         </xs:appinfo>
         <xs:documentation>Transport eines Vorgangs (Antrag/Fall/Bescheid) zwischen den Partnern. Dies ist immer die erste Nachricht für die Abwicklung eines kompletten, fallbezogenen Antragsverfahren. Diese Nachricht wird zum Beispiel zur Übermittlung des Online-Antrages für folgenden Leistungskombinationen genutzt: Antrag auf Waffenbesitzkarte (grün) mit Erwerbserlaubnis (Voreintrag) für eine oder mehrere Waffen (Bedürfnis: Jäger, Sportschütze oder gefährdete Person) Antrag auf Waffenbesitzkarte (grün) mit Eintragung schon erworbener Waffen (Jungjäger) (Bedürfnis: Jäger) Antrag auf Waffenbesitzkarte (gelb) Antrag auf Waffenbesitzkarte (rot)</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xewaffe:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="vorgang" type="xewaffe:Vorgang">
                     <xs:annotation>
                        <xs:documentation>Angaben zum Vorgang (Antrag/Fall/Bescheid)</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="vorgang.nachricht.2020">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>§21 WaffG; §37a-d WaffG nF; §37f WaffG nF; §9 (2) WaffRG</rechtsgrundlage>
            <title>Freie Nachricht transportieren</title>
         </xs:appinfo>
         <xs:documentation>Diese Nachricht dient zum Transport von Informationen in Freitext, zum Beispiel anstatt einer E-Mail oder eines Telefonanrufes, oder zur Übermittlung von Unterlagen zu einem bereits existierenden Vorgang. Dieser Nachricht muss zuerst eine Nachricht vom Typ vorgang.transportieren.2010 vorausgegangen sein. Der Vorgang, auf den sich diese Nachricht bezieht, wird mit dem Element identifikationVorgang referenziert. Die Nachricht, auf die Bezug genommen wird, ist im Feld nachrichtenkopf/referenzUUID anzugeben.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xewaffe:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="identifikationVorgang" type="xewaffe:Identifikation.Vorgang">
                     <xs:annotation>
                        <xs:documentation>Eindeutige Identifizierung des Vorgangs, auf den Bezug genommen werden soll</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="freieNachricht" minOccurs="0" type="xewaffe:FreieNachricht">
                     <xs:annotation>
                        <xs:documentation>Typ zur Übertragung von Freitextinformationen</xs:documentation>
                     </xs:annotation>
                  </xs:element>
                  <xs:element name="anlage"
                              minOccurs="0"
                              maxOccurs="unbounded"
                              type="xewaffe:Anlage">
                     <xs:annotation>
                        <xs:documentation>Zum Vorgang oder zur freien Nachricht gehörige Unterlage(n)</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
   <xs:element name="vorgang.statusabfrage.2030">
      <xs:annotation>
         <xs:appinfo>
            <rechtsgrundlage>§21 WaffG; §37a-d WaffG nF; §37f WaffG nF; §9 (2) WaffRG</rechtsgrundlage>
            <title>Status abfragen</title>
         </xs:appinfo>
         <xs:documentation>Mit dieser Nachricht wird der aktuelle fachliche Status eines Vorgangs explizit angefragt. Der Empfänger der Nachricht antwortet auf diese Nachricht mit der Nachricht administration.quittung.0020 und gibt dann im Element quittung-&gt;aktuellerStatusFachlich den aktuellen fachlichen Status zurück.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:complexContent>
            <xs:extension base="xewaffe:Nachricht.G2G">
               <xs:sequence>
                  <xs:element name="identifikationVorgang" type="xewaffe:Identifikation.Vorgang">
                     <xs:annotation>
                        <xs:documentation>Hier wird die eindeutige Kennung des Vorgangs angegeben, zu welchem der Status abgefragt werden soll.</xs:documentation>
                     </xs:annotation>
                  </xs:element>
               </xs:sequence>
            </xs:extension>
         </xs:complexContent>
      </xs:complexType>
   </xs:element>
</xs:schema>
