# Standard XeWaffe

**`XeWaffe V1.1.0`** ist die erste **`XeWaffe`** Veröffentlichung mit Versionsstatus ”Endfassung” (Kategorie Fassung).

Um eine direkte Einbindung der aktuellen [***XWaffe*** Version](https://www.xrepository.de/details/urn:xoev-de:bmi:standard:xwaffe), dem XÖV Standard zu Datenübermittlungen der Waffenbehörden an das Nationalen Waffenregisters (NWR), zu ermöglichen, wird hier auch die ***XWaffe V2.5*** Schema-Definition abgelegt. Dies ist notwendig, da die Schema Location von ***XWaffe*** auf einer Pseudo-URL basiert und zur Integration durch einen lokalen Pfad ersetzt werden muss.
