# Standard XeWaffe

**`XeWaffe`** ist ein XÖV **Datenaustauschstandard** für die Elektronische Datenübermittlung für waffenrechtliche Erlaubnisse. Der Standard **`XeWaffe`** deckt den Bedarf des Betreibers sowie den weiterer Stakeholder am Transport von digitalen (OZG-) Anträgen im Kontext waffenrechtlicher Erlaubnisse ab.

Die XML Schema-Definitionen eines XÖV-Standards basieren auf der von dem W3C empfohlenen XML Schema-Definitionssprache.
Alle im Rahmen eines XÖV-Standards definierten globalen XML-Elemente und benannten XML-Typen müssen sich in einem Namensraum befinden, der den betroffenen XÖV-Standard eindeutig identifiziert. Jede XML Schema-Definition eines XÖV-Standards muss versioniert sein.

Das XÖV-Handbuch empfiehlt, den physischen Speicherort einer XML Schema-Definition in Form einer öffentlichen URL anzugeben. Die hier vorliegende versionierte Ablage der **`XeWaffe`** Schema-Definitionen dient genau dem Zweck, einen öffentlicher Zugang zu den XML Schema-Definitionen des XÖV-Standards **`XeWaffe`**  bereitzustellen und XML Schema-Validatoren einen direkten Zugriff zu ermöglichen.

Um eine direkte Einbindung der aktuellen [***XWaffe*** Version](https://www.xrepository.de/details/urn:xoev-de:bmi:standard:xwaffe), dem XÖV Standard zu Datenübermittlungen der Waffenbehörden an das Nationalen Waffenregisters (NWR), zu ermöglichen, wird hier auch die ***XWaffe*** Schema-Definition abgelegt. Dies ist notwendig, da die Schema Location von ***XWaffe*** auf einer Pseudo-URL basiert und zur Integration durch einen lokalen Pfad ersetzt werden muss.

## Beispiele der Antragstrecken

Auf der Seite [AKDB Digitale Verwaltung as a Service](https://digitale-verwaltung-as-a-service.de/dienste-demos/) kann auf die bereits im produktiven Einsatz als auch auf die noch in der Weiterentwicklung sich befindenden Dienste zugegriffen werden.

## Releases

Version: 1.5.0

Releases des Standards **`XeWaffe`** sind im [XRepository](https://www.xrepository.de/details/urn:xoev-de:stmd:standard:xewaffe) abgelegt. Der Herausgeber ist die [AKDB - Anstalt für Kommunale Datenverarbeitung in Bayern](https://www.akdb.de/).

Die Releases des Standards **`XeWaffe`** umfassen folgende Bestandteile:

- **UML-Fachmodell (XMI)**:
  Das UML-Modell enthält die Datenstrukturen mit den entsprechenden semantischen Definitionen.

- **XML-Schema-Definitionen (XSD):**
  Die XML-Schemadateien definieren das XML-Format der **`XeWaffe`** Antragsdaten.

- **Spezifikation (PDF):**
  Die **`XeWaffe`** Spezifikation beinhaltet unter anderem das Datenstrukturmodell, die Beschreibung der Nachrichten, das Informationsmodell  und die Codelisten von **`XeWaffe`**.
  Die **`XeWaffe`** Spezifikation stellt im Anhang F die Versionshistorie zur Verfügung. Die Unterkapitel von Anhang F beinhalten die Release Notes für alles bisherigen **`XeWaffe`** Versionen. Dort werden alle Änderungen zur jeweiligen **`XeWaffe`** Vorgängerversion aufgelistet. 

## Lizenzen

Die XÖV Standards sind, entsprechend dem XÖV Handbuch, offene und lizenzkostenfreie Standards, die allen Interessierten frei zugänglich zur Verfügung stehen, somit Open-Source. Die zum **`XeWaffe`** Standard passende Lizenz (ggf. European Union Public Licence (EUPL)) muss noch ausgewählt und angegeben werden. Dies ist derzeit noch in Klärung mit der [Koordinierungsstelle für IT-Standards (KoSIT)](https://www.xoev.de/).

## Kontakt

Mailkontakt: ewaffe@akdb.de
