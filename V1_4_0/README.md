# Standard XeWaffe Version 1.4.0

**Spezifikation von XeWaffe**

Die aktuelle Spezifikation ist [hier](XeWaffe-Spezifikation_v1.4.0_2024-08-14.pdf "XeWaffe Spezifikation v1.4.0")
zu finden.
