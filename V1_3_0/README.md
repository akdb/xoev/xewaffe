# Standard XeWaffe Version 1.3.0

**Spezifikation von XeWaffe**

Die aktuelle Spezifikation ist [hier](XeWaffe-Spezifikation_v1.3.0_2024-01-16.pdf "XeWaffe Spezifikation v1.3.0")
zu finden.

**Integration von XWaffe 2.6**

Um eine direkte Einbindung der aktuellen [XWaffe
Version 2.6](https://www.xrepository.de/details/urn:xoev-de:bmi:standard:xwaffe),
dem XÖV Standard zur Datenübermittlung der Waffenbehörden an das Nationale
Waffenregister (NWR), zu ermöglichen, wird hier auch die Schemadefinition von
**XWaffe Version 2.6** abgelegt. Dies ist notwendig, da die XML Schema Location von
**XWaffe** auf einer Pseudo-URL basiert und zur Integration durch eine real
existierende URL ersetz werden muss, unter der die Schemadefinitionsdateien zu
finden sind.
