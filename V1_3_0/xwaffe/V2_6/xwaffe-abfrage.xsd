<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:xwaffe-behoerden="http://www.xwaffe.de/schemata/behoerden/V2_6/"
           xmlns:xwaffe-kern="http://www.xwaffe.de/schemata/kern/V2_6/"
           targetNamespace="http://www.xwaffe.de/schemata/behoerden/V2_6/"
           version="2.6"
           elementFormDefault="qualified"
           attributeFormDefault="unqualified">
   <xs:annotation>
      <xs:appinfo>
         <standard>
            <nameLang>XWaffe – Standard für die elektronische Datenübermittlung im deutschen Waffenwesen</nameLang>
            <nameKurz>XWaffe</nameKurz>
            <nameTechnisch>xwaffe</nameTechnisch>
            <kennung>urn:xoev-de:bmi:standard:xwaffe</kennung>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Grundlage der Kommunikation ist das Waffengesetz (WaffG) sowie das Waffenregistergesetz (WaffRG), sowie die zugehörigen Rechtsverordnungen (WaffRGDV). Mit Stand des WaffG und WaffRG vom 01. September 2020 werden die Meldepflichten von Inhabern waffenrechtlicher Erlaubnisse gem. §21 Abs. 1 WaffG (Händler, Hersteller) sowie die Übermittlungsanlässe der Waffenebehörden an das Nationalen Waffenregisters (NWR) unterstützt.</beschreibung>
         </standard>
         <versionStandard>
            <version>2.6</version>
            <beschreibung>Zur Unterstützung der effizienten und wirtschaftlichen Umsetzung durchgehender medienbruchfreier elektronischer Prozesse im deutschen Waffenwesen wird der Standard XWaffe erstellt. Grundlage der Kommunikation ist das Waffengesetz (WaffG) sowie das Waffenregistergesetz (WaffRG), sowie die zugehörigen Rechtsverordnungen (WaffRGDV). Mit Stand des WaffG und WaffRG vom 01. September 2020 werden die Meldepflichten von Inhabern waffenrechtlicher Erlaubnisse gem. §21 Abs. 1 WaffG (Händler, Hersteller) sowie die Übermittlungsanlässe der Waffenebehörden an das Nationalen Waffenregisters (NWR) unterstützt.</beschreibung>
            <versionXOEVProfil>3.0.1</versionXOEVProfil>
            <versionXOEVHandbuch>3.0.1</versionXOEVHandbuch>
            <versionXGenerator>3.1.1</versionXGenerator>
            <versionModellierungswerkzeug>19.0 SP4</versionModellierungswerkzeug>
            <nameModellierungswerkzeug>MagicDraw</nameModellierungswerkzeug>
         </versionStandard>
      </xs:appinfo>
      <xs:documentation>Nachrichtentypen für die Such- und Rechercheabfragen von Informationen aus dem Zentralen Waffenregister. Dieser Nachrichtentyp kann von den ÖWS oder von berechtigten Stellen zur Abfrage verwendet werden.</xs:documentation>
   </xs:annotation>
   <xs:include schemaLocation="xwaffe-baukasten-behoerden.xsd"/>
   <xs:import schemaLocation="xwaffe-baukasten.xsd"
              namespace="http://www.xwaffe.de/schemata/kern/V2_6/"/>
   <xs:element name="abfrage.auskunft.401">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 WaffRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Suchanfrage des ÖWS oder berechtigter Stellen an das NWR</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:Nachrichtenkopf">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="suchprofil" type="xwaffe-behoerden:Suchprofil">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstigen berechtigten Stellen durchgeführten Suchabfragedaten ab. Übergreifende Suchparameter werden im Suchprofil aufgenommen, fachspezifische (z.B. Tag der Geburt) in den Datenfeldern pro gesuchtes Objekt (Person, etc.).</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.lesen.405">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 WaffRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage von gespeicherten Datensätzen anhand ihrer Ordnungsnummer (NWRID)</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:Nachrichtenkopf">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="leseprofil"
                        type="xwaffe-behoerden:Leseprofil"
                        form="unqualified">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstige berechtigte Stellen durchgeführten Leseabfragedaten ab.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.person.aktualisieren.407">
      <xs:annotation>
         <xs:appinfo>
            <autor>ÖWS, Kopfstelle des NWR</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Die zuständige Waffenbehörde kann die Aktualisierung der allgemeinen Personendaten über das BZSt mit dieser Nachricht jederzeit anfordern. Die Waffenbehörde erhält als Rückmeldung antwort.lesen.406 mit den aktualisierten Personendaten. Eine Aktualisierungsanfrage darf nur bei bestehendem „fachlichem Bedarf“ (§2 Nr.2 IDNrG) ausgeführt werden.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:Nachrichtenkopf">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="personID" type="xwaffe-kern:String.nwrID">
               <xs:annotation>
                  <xs:documentation>Jedes Personenobjekt erhält für die interne Verwaltung im NWR eine Personenidentifikationsnummer (Ordnungsnummer im Sinne des WaffRG). Die Personenidentifikationsnummer ist NWR-weit eindeutig und dient zur Referenzierung auf das konkrete Personenobjekt innerhalb des NWR. Eine Person (natürliche oder nichtnatürliche) kann durch Anlage von Dubletten bei verschiedenen örtlichen Waffenverwaltungssystemen in mehreren Personenobjekten abgebildet sein. Bildungsregel der ID siehe unter String.nwrID.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.recherche.411">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 WaffRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage zur Ermittlung der einem Hauptobjekt (Person, Erlaubnis, Waffe, Waffenteil) untergeordneten Datensätze</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:Nachrichtenkopf">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="rechercheprofil" type="xwaffe-behoerden:Rechercheprofil">
               <xs:annotation>
                  <xs:documentation>Dieses Objekt bildet die durch die ÖWS und sonstige berechtigte Stellen durchgeführten Recherchen mit den zugehörigen Abfragedaten ab. Die Rechercheparameter werden über das Rechercheprofil aufgenommen. Die Recherche erfolgt dabei grundsätzlich über ObjektIDs. In den Datenfeldern können pro zu filterndem Objekt (Erlaubnis, Waffe, etc.) zusätzlich Angaben enthalten sein, die die Recherche eingrenzen. Zurückgeliefert werden bei einer Recherche mit: PersonID: Die vorhandenen Erlaubnisobjekte (Anträge und erteilte Erlaubnisse als Erlaubnisinhaber / wirtschaftlich berechtigter Kaufmann oder Unternehmer bzw. als Verantwortlicher oder Erlaubnisinhaber nach §21 Absatz 1 Satz 1 WaffG) oder die im Erwerbsprozess an diese Person befindlichen Waffen oder Waffenteilobjekte. PersonenstammID: Die vorhandenen Personenobjekte. ErlaubnisID: Die vorhandenen Waffen-, Waffenteil, Waffenverweis-, VoreintragsWaffenteil oder Voreintragsobjekte. WaffenID: Die vorhandenen Waffenteil-, Aktivitäts-, Waffenverweis- oder Voreintragsobjekte. WaffenteilID: Die vorhandenen Waffenteil-, Aktivitäts-, Waffenverweis- oder VoreintragWaffenteilobjekte.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="startIndex" minOccurs="0" type="xs:integer">
               <xs:annotation>
                  <xs:documentation>Der StartIndex dient der Paginierung. Bei der (ersten) Abfrage (Auskunft, Recherche) ist der StartIndex 0 bzw. braucht er nicht übermittelt werden. Als Ergebnis bekommt das abfragende System die Trefferliste beginnend ab dem mit der Abfrage angegebenen StartIndex (hier 0) sowie die Angabe der Gesamtanzahl der Treffer der gesamten Abfragen(z.B. 543). Zurückgeliefert wird eine systemseitig festlegte max. Anzahl von Treffern (z.B. 100) pro Abfrage.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
   <xs:element name="abfrage.hinweis.421">
      <xs:annotation>
         <xs:appinfo>
            <autor>Abfrageberechtigte Systeme auf Basis §10 WaffRG</autor>
            <leser>Registerbehörde NWR</leser>
         </xs:appinfo>
         <xs:documentation>Abfrage der Ordnungsnummern (NWRID) der für eine WaffB hinterlegten Hinweise.</xs:documentation>
      </xs:annotation>
      <xs:complexType>
         <xs:sequence>
            <xs:element name="kopf" type="xwaffe-behoerden:Nachrichtenkopf">
               <xs:annotation>
                  <xs:documentation>Der Nachrichtenkopf umfasst allgemeine, technische Eigenschaften einer Nachricht, wie z.B. Erstellungszeitpunkt oder Tagesnachrichtenzähler.</xs:documentation>
               </xs:annotation>
            </xs:element>
            <xs:element name="hinweisprofil" type="xwaffe-behoerden:Hinweisprofil">
               <xs:annotation>
                  <xs:documentation>Daten zur Einschränkung der gewünschten Hinweismenge.</xs:documentation>
               </xs:annotation>
            </xs:element>
         </xs:sequence>
      </xs:complexType>
   </xs:element>
</xs:schema>
